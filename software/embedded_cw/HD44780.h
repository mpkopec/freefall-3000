/*
 * HD44780.h
 *
 *  Created on: 16 pa� 2014
 *      Author: Maciek
 */

#ifndef HD44780_H_
#define HD44780_H_

// --------------------------------
#define HD44780_DPORT PORTD
#define HD44780_DDDR DDRD
#define HD44780_DPIN PIND
#define HD44780_RSPORT PORTB
#define HD44780_RSDDR DDRB
#define HD44780_RWPORT PORTC
#define HD44780_RWDDR DDRC
#define HD44780_EPORT PORTC
#define HD44780_EDDR DDRC
#define HD44780_RS 0
#define HD44780_RW 3
#define HD44780_E 4
#define HD44780_RS_SET (HD44780_RSPORT |= 1 << HD44780_RS)
#define HD44780_RW_SET (HD44780_RWPORT |= 1 << HD44780_RW)
#define HD44780_E_SET (HD44780_EPORT |= 1 << HD44780_E)
#define HD44780_RS_RESET (HD44780_RSPORT &= ~(1 << HD44780_RS))
#define HD44780_RW_RESET (HD44780_RWPORT &= ~(1 << HD44780_RW))
#define HD44780_E_RESET (HD44780_EPORT &= ~(1 << HD44780_E))

#define HD44780_ENABLE_DDR DDRC
#define HD44780_ENABLE_PORT PORTC
#define HD44780_ENABLE PC1
// --------------------------------

#include <avr/io.h>
#include <avr/delay.h>

void HD44780_outPorts ();
void HD44780_inPorts ();
void HD44780_sendCommand (uint8_t cmd);
uint8_t HD44780_readStatus ();
void HD44780_sendData (uint8_t data);
void HD44780_setAddress (uint8_t line, uint8_t charPos);
void HD44780_returnHome ();
void HD44780_clearDisplay ();
void HD44780_init (uint8_t cb);
void HD44780_enable ();
void HD44780_disable ();
void HD44780_sendString (char *str);

#endif /* HD44780_H_ */
