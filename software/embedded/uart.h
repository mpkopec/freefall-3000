/*
 * uart.h
 *
 *  Created on: 30 lis 2014
 *      Author: Maciek
 */

#ifndef UART_H_
#define UART_H_

#define FOSC 12000000
#define BAUD 115200
#define UBRR_TO_SET (FOSC/8/BAUD-1)

#define UART_ENABLE_PORT PORTC
#define UART_ENABLE_DDR DDRC
#define UART_ENABLE PC2

#include <avr/io.h>

void UART_init();
void UART_sendByte(unsigned char data);
void UART_sendBytes(unsigned char *data, uint8_t nBytes);
void UART_sendString(const char *str);
uint8_t UART_receive(void);
void UART_flush(void);
void UART_disable();
void UART_enable();

void inline UART_enableReceiverInterrupt () {
	UCSRB |= (1<<RXCIE);
}

void inline UART_disableReceiverInterrupt () {
	UCSRB &= ~(1<<RXCIE);
}

#endif /* UART_H_ */
