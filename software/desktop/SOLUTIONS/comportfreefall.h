#ifndef COMPORTFREEFALL_H
#define COMPORTFREEFALL_H

#include <windows.h>

#ifdef BUILDING_DLL
#define EXPORT __declspec (dllexport)
#else
#define EXPORT __declspec (dllimport)
#endif

EXPORT int lib_RS232_OpenComport(int comport_number, int baudrate, const char *mode);
EXPORT int lib_RS232_PollComport(int comport_number, unsigned char *buf, int size);
EXPORT int lib_RS232_SendByte(int comport_number, unsigned char byte);
EXPORT int lib_RS232_SendBuf(int comport_number, unsigned char *buf, int size);
EXPORT void lib_RS232_CloseComport(int comport_number);
EXPORT void lib_RS232_cputs(int comport_number, const char *text);
EXPORT void lib_RS232_nputs(int comport_number, const char *text);

EXPORT void lib_RS232_commlistener(int comport_number,
                                   bool * data_ready,
                                   int * error,
                                   bool * reading,
                                   unsigned char * buffer,
                                   int * buffer_index);

EXPORT void lib_RS232_datalistener(int comport_number,
                                   unsigned char * buffer,
                                   int * buffer_index);


#endif // COMPORTFREEFALL_H
