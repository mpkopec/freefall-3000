/*
 * servo.h
 *
 *  Created on: 30 lis 2014
 *      Author: Maciek
 */

#ifndef SERVO_H_
#define SERVO_H_

#include <avr/io.h>
#include <avr/interrupt.h>

void SERVO_initOperation ();
void SERVO_goUpPWM ();
void SERVO_goDownPWM ();
void SERVO_disablePWM ();
void SERVO_startCountingPulses ();
void SERVO_stopCountingPulses ();
void SERVO_shutterToggle ();

#endif /* SERVO_H_ */
