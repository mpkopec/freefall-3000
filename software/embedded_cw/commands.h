/*
 * commands.h
 *
 *  Created on: 4 sty 2015
 *      Author: maciek
 */

#ifndef COMMANDS_H_
#define COMMANDS_H_

#include <avr/io.h>

typedef struct Command {
	const char *id;
	void (*handler)();
} Command;

typedef struct CommandList {
	uint8_t n;
	uint8_t allocatedCapacity;
	Command *cList;
} CommandList;

typedef struct Flags {
	uint8_t receivedCommand:1,
			commandCharCounter:3,
			dataAquisitionFinished:1,
			undefinedFlag:3;
} Flags;

typedef enum CommandIndex {
	CF_ID, // 0 - Device identification query
	RQ_WF, // 1 - Data request - waveform only
	RQ_PK, // 2 - Data request - peaks only
	RQ_WP, // 3 - Data request - waveform and peaks
	MS_ST, // 4 - Start measurement
	CF_ST, // 5 - Current device status
	SY_TG, // 6 - Shutter toggle
	SY_MS, // 7 - Memory status register value
	CF_AK, // 8 - Acknowledgment received
	SY_UP, // 9 - Shutter up
	SY_DN  // 10 - Shutter down
} CommandIndex;

typedef enum State {
	CF_IL, // Idle state, after start-up or after reading measurement from the device
	CF_RD, // Data ready state, data was measured, peaks were calculated
	CF_CS, // Data collection start state, ADC data collection is started
	CF_CF, // Data collection finish state, ADC data collection is finished
	CF_UP  // Device is being initialized
} State;

extern const char *allPossibleCommands[];
extern const char *allPossibleStates[];
extern char cmd[6];
extern volatile Flags flagsHolder;
extern volatile State state;

CommandList *newCommandList(uint8_t initialCapacity);
void deleteCommandList(CommandList *cList);
void resizeCommandList(CommandList *cList, uint8_t newCapacity);
void addCommand(CommandList *cList, Command cmd);


#endif /* COMMANDS_H_ */
