/*
 * spi.c
 *
 *  Created on: 14 gru 2014
 *      Author: maciek
 */

#include "spi.h"

void SPI_masterInit(void) {
	/* Set MOSI and SCK output, all others input */
	DDRB |= (1<<PB3)|(1<<PB5);
	DDRB &= ~(1<<PB4);
	/* Enable SPI, Master, set clock rate fck/2 */
	SPCR = (1<<SPE)|(1<<MSTR);
	SPSR = (1<<SPI2X);
}
