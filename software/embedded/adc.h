/*
 * adc.h
 *
 *  Created on: 2 sty 2015
 *      Author: maciek
 */

#ifndef ADC_H_
#define ADC_H_

#define THRESHOLD 127
#define HIST 3
#define PEAK_COUNT 18

#include <avr/io.h>

typedef struct Peaks {
	uint32_t *peakList;
	uint8_t peakCount;
} Peaks;

void ADC_init();
uint16_t ADC_sample();
Peaks *ADC_findPeaks();

inline void ADC_startConversion () {
	ADCSRA |= (1<<ADSC);
}

inline void ADC_enableInterrupt () {
	ADCSRA |= (1<<ADIE);
}

inline void ADC_disableInterrupt () {
	ADCSRA &= ~(1<<ADIE);
}

inline void ADC_setFreeRunning () {
	ADCSRA |= (1<<5);
}

inline void ADC_resetFreeRunning () {
	ADCSRA &= ~(1<<5);
}

#endif /* ADC_H_ */
