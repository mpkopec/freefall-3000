/*
 * servo.c
 *
 *  Created on: 30 lis 2014
 *      Author: Maciek
 */

#include "servo.h"

volatile uint8_t servoPulses = 0;

void SERVO_initOperation () {
	DDRB |= (1<<PB1)|(1<<PB2);
	TCCR1A = 0b10100010;
	TCCR1B = 0b00011101;
	ICR1 = 240;
}

void SERVO_goUpPWM () {
	OCR1A = OCR1B = 9;
}

void SERVO_goDownPWM () {
	OCR1A = OCR1B = 16;
}

void SERVO_disablePWM () {
	TCCR1B = 0b00011000;
}

void SERVO_startCountingPulses () {
	TCCR0 = 0x05;
	TIMSK |= 0x01;
}

void SERVO_stopCountingPulses () {
	TCCR0 = 0x00;
	TIMSK &= ~0x01;
}

void inline SERVO_shutterToggle () {
	SERVO_startCountingPulses();
}

ISR(TIMER0_OVF_vect) {
	if (servoPulses == 0) {
		SERVO_goDownPWM();
	} else if (servoPulses == 30) {
		SERVO_goUpPWM();
	} else if (servoPulses == 60) {
		servoPulses = 0;
		SERVO_stopCountingPulses();
		return;
	}

	servoPulses++;
}
