/*
 * main.c
 *
 *  Created on: 26 pa� 2014
 *      Author: Maciek
 */

#define F_CPU 12000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>

#include "HD44780.h"

void ADC_init () {
	ADMUX = 0x41; // Vref from AVCC, ADC1 as an input
	ADCSRA = (1 << ADEN) | 7; // 128 clock divider which gives 93 kHz sampling rate, ADCEN flag set
}

uint16_t ADC_convert () {
	ADCSRA |= 1 << ADSC;
	while (!(ADCSRA & (1 << ADIF)));
	ADCSRA = 0x87;

	uint16_t converted = ADCL;
	uint16_t tmp = ADCH << 8;
	converted |= tmp;
	converted &= ~0xfc00;

	return converted;
}

int main (void) {
	char str[17];

	HD44780_init(0x00);
	ADC_init();

	while (1) {
		HD44780_clearDisplay();
		HD44780_setAddress(1, 0);
		uint16_t adc = ADC_convert();
		sprintf(str, "%d", adc);
		HD44780_sendString(str);

		_delay_ms(200);
	}

	return 0;
}
