/*
 * spi.h
 *
 *  Created on: 14 gru 2014
 *      Author: maciek
 */

#ifndef SPI_H_
#define SPI_H_

#include <avr/io.h>

void SPI_masterInit(void);
inline void SPI_masterTransmit (uint8_t data) {
	/* Start transmission */
	SPDR = data;
	while(!(SPSR & (1<<SPIF))) {
		asm("");
	}
}
inline uint8_t SPI_masterReceive () {
	SPDR = 0x00;
	while(!(SPSR & (1<<SPIF))) {
		asm("");
	}
	return SPDR;
}
inline uint8_t SPI_masterFullDuplex (uint8_t data) {
	/* Start transmission */
	SPDR = data;
	/* Wait for transmission complete */
	while(!(SPSR & (1<<SPIF))) {
		asm("");
	}
	return SPDR;
}

#endif /* SPI_H_ */
