/*
 * adc.c
 *
 *  Created on: 2 sty 2015
 *      Author: maciek
 */


#include "adc.h"
#include <avr/interrupt.h>
#include <stdlib.h>
#include "uart.h"
#include "commands.h"
#include "mem_23LCV1024.h"

void ADC_init () {
	ADMUX = 0xe0;
//	ADCSRA = 0x83; // prescaler - 8; 114 kSps; time unit - 8.77 us
	ADCSRA = 0x84; // prescaler - 16; 58 kSps; time unit - 17.24 us
//	ADCSRA = 0x85; // prescaler - 32; 28.85 kSps; time unit - 34.67 us
	/*
	 * First conversion is over twice as long, so to make the device to measure
	 * with assumed sample rate, at least one dummy sampling need to be performed
	 * before turning on the free running mode to avoid time measurement error.
	 * Two samplings are performed, just in case.
	 */
	ADC_sample();
	ADC_sample();
}

uint16_t ADC_sample () {
	ADC_startConversion();

	while (!(ADCSRA & (1<<ADIF))) {
		asm("");
	}

	uint16_t result = ADC;
	return result;
}

Peaks *ADC_findPeaks () {
	Peaks *peaks = calloc(1, sizeof(Peaks));
	if (!peaks) {
		return NULL;
	}
	uint32_t lastThreshold = 0;
	uint32_t pulseWidth = 0;
	uint8_t value = 0;
	uint8_t st = 0;

	peaks->peakList = (uint32_t *) calloc(PEAK_COUNT, sizeof(uint32_t));

	mem_23LCV1024_setRwStatus(1);
	mem_23LCV1024_moveToAddress(0);

	value = mem_23LCV1024_readByte();

	if (value < THRESHOLD) {
		st = 0;
	} else {
		st = 1;
	}

	while (mem_23LCV1024_addrCtr <= 0x1ffffUL) {
		value = mem_23LCV1024_readByte();

		if (value < THRESHOLD - HIST && st == 1) {
			lastThreshold = mem_23LCV1024_addrCtr - 1UL;
			st = 0;
		} else if (value > THRESHOLD + HIST && st == 0) {
			st = 1;
		}

		if (st == 0 && lastThreshold != 0UL) {
			pulseWidth++;
		} else {
			if (pulseWidth != 0UL) {
				peaks->peakList[peaks->peakCount++] = lastThreshold + pulseWidth/2UL;
				pulseWidth = 0UL;
				lastThreshold = 0UL;
			}
		}
	}

	uint8_t i;
	for (i = peaks->peakCount; i < PEAK_COUNT; i++) {
		peaks->peakList[i] = 0xff000000UL;
	}

	return peaks;
}

ISR(ADC_vect) {
	uint8_t adc_res = ADCH;
	if (mem_23LCV1024_addrCtr == 0x1ffff) {
		ADC_resetFreeRunning();
		ADC_disableInterrupt();
		flagsHolder.dataAquisitionFinished = 1;
	}

	mem_23LCV1024_writeByte(adc_res);
}
