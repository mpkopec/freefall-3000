/*
 * uart.c
 *
 *  Created on: 30 lis 2014
 *      Author: Maciek
 */

#include "uart.h"

void UART_disable () {
	UART_ENABLE_DDR = 1 << UART_ENABLE;
	UCSRB &= ~((1<<RXEN)|(1<<TXEN));
	UART_ENABLE_PORT |= 1 << UART_ENABLE;
}

void UART_enable () {
	UART_ENABLE_DDR = 1 << UART_ENABLE;
	UART_ENABLE_PORT &= ~(1 << UART_ENABLE);
	UCSRA |= (1<<U2X);
	UCSRB |= (1<<RXEN)|(1<<TXEN);
}

void UART_init() {
	UART_enable();
	/* Set baud rate */
	UBRRH = (unsigned char)(UBRR_TO_SET>>8);
	UBRRL = (unsigned char)UBRR_TO_SET;
	/* Enable receiver and transmitter */
	UCSRB = (1<<RXEN)|(1<<TXEN);
	/* Set frame format: 8data, 2stop bit */
	UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);
}

void UART_sendByte(unsigned char data) {
	/* Wait for empty transmit buffer */
	while ( !( UCSRA & (1<<UDRE)) ) {
		asm("");
	}
	/* Put data into buffer, sends the data */
	UDR = data;
	while ( !( UCSRA & (1<<TXC)) ) {
		asm volatile("");
	}
}

void UART_sendBytes (unsigned char *data, uint8_t nBytes) {
	uint8_t i;
	for (i = 0; i < nBytes ; i++) {
		UART_sendByte(data[i]);
	}
}

uint8_t UART_receive(void) {
	/* Wait for data to be received */
	while ( !(UCSRA & (1<<RXC)) ) {
		asm("");
	}
	/* Get and return received data from buffer */
	return UDR;
}

void UART_flush( void ) {
	unsigned char dummy;
	while ( UCSRA & (1<<RXC) ){
		dummy = UDR;
	}
}

void UART_sendString (const char *str) {
	do {
		UART_sendByte(*str);
	} while(*(++str));
}
