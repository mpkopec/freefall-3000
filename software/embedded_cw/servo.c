/*
 * servo.c
 *
 *  Created on: 30 lis 2014
 *      Author: Maciek
 */

#include "servo.h"

volatile uint8_t servoPulses = 0;

void SERVO_initOperation () {
	
}

void SERVO_goUpPWM () {
	
}

void SERVO_goDownPWM () {
	
}

void SERVO_disablePWM () {
	TCCR1B = 0b00011000;
}

void SERVO_startCountingPulses () {
	TCCR0 = 0x05;
	TIMSK |= 0x01;
}

void SERVO_stopCountingPulses () {
	TCCR0 = 0x00;
	TIMSK &= ~0x01;
}

void inline SERVO_shutterToggle () {
	SERVO_startCountingPulses();
}

ISR(TIMER0_OVF_vect) {
	if (servoPulses == 0) {
		SERVO_goDownPWM();
	} else if (servoPulses == 30) {
		SERVO_goUpPWM();
	} else if (servoPulses == 60) {
		servoPulses = 0;
		SERVO_stopCountingPulses();
		return;
	}

	servoPulses++;
}
