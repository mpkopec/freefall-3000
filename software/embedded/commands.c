/*
 * commands.c
 *
 *  Created on: 4 sty 2015
 *      Author: maciek
 */


#include "commands.h"
#include <avr/interrupt.h>
#include <stdlib.h>

/**
 * \var allPossibleCommands
 * Array containing pointers to a string database of possible commands,
 * which may be received over RS-232.
 */
const char *allPossibleCommands[] = {
		"CF_ID", // 0 - Device identification query
		"RQ_WF", // 1 - Data request - waveform only
		"RQ_PK", // 2 - Data request - peaks only
		"RQ_WP", // 3 - Data request - waveform and peaks
		"MS_ST", // 4 - Start measurement
		"CF_ST", // 5 - Current device status
		"SY_TG", // 6 - Shutter toggle
		"SY_MS", // 7 - Memory status register value
		"CF_AK", // 8 - Acknowledgment received
		"SY_UP", // 9 - Shutter up
		"SY_DN"  // 10 - Shutter down
};

/**
 * \var allPossibleStates
 * All possible states in string format.
 * Indices correspond to State enumeration value.
 */
const char *allPossibleStates[] = {
		"CF_IL\n",
		"CF_RD\n",
		"CF_CS\n",
		"CF_CF\n",
		"CF_UP\n"
};

volatile Flags flagsHolder = {.receivedCommand = 0, .commandCharCounter = 0,
		.dataAquisitionFinished = 0, .undefinedFlag = 0};
volatile State state = CF_UP;
char cmd[6];

CommandList *newCommandList (uint8_t initialCapacity) {
	if (initialCapacity < 1) {
		initialCapacity = 10;
	}

	CommandList *newCList = malloc(sizeof(CommandList));
	if (newCList == NULL) {
		return NULL;
	}

	Command *cList = malloc(sizeof(Command) * initialCapacity);
	if (newCList == NULL) {
		return NULL;
	}

	newCList->n = 0;
	newCList->allocatedCapacity = initialCapacity;
	newCList->cList = cList;

	return newCList;
}

void resizeCommandList (CommandList *cList, uint8_t newCapacity) {
	if (newCapacity > cList->allocatedCapacity) {
		Command *newCList = realloc(cList->cList, sizeof(Command) * newCapacity);
		if (newCList == NULL) {
			return;
		} else {
			cList->cList = newCList;
			cList->allocatedCapacity = newCapacity;
		}
	}
}

void deleteCommandList (CommandList *cList) {
	free(cList->cList);
	free(cList);
}

void addCommand (CommandList *cList, Command cmd) {
	if (cList->n == cList->allocatedCapacity) {
		resizeCommandList(cList, cList->allocatedCapacity + 1);
	}

	cList->cList[cList->n] = cmd;
	cList->n++;
}

ISR (USART_RXC_vect) {
	uint8_t nextChar = UDR;

	if (nextChar != '\n') {
		if (flagsHolder.commandCharCounter > 4) {
			flagsHolder.commandCharCounter = 0;
			reti();
		}

		cmd[flagsHolder.commandCharCounter++] = nextChar;
	} else {
		cmd[flagsHolder.commandCharCounter] = '\0';
		flagsHolder.commandCharCounter = 0;
		flagsHolder.receivedCommand = 1;
	}
}
