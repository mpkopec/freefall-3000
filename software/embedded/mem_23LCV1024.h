/*
 * mem_23LCV1024.h
 *
 *  Created on: 14 gru 2014
 *      Author: maciek
 */

#ifndef MEM_23LCV1024_H_
#define MEM_23LCV1024_H_

#include "spi.h"
#include <avr/io.h>

#define MEM_CS_DDR DDRC
#define MEM_CS_PORT PORTC
#define MEM_CS_PIN 5
#define MEM_SELECT (MEM_CS_PORT &= ~(1<<MEM_CS_PIN))
#define MEM_DESELECT (MEM_CS_PORT |= (1<<MEM_CS_PIN))

#define MEM_CMD_READ 0x03
#define MEM_CMD_WRITE 0x02
#define MEM_CMD_EDIO 0x3B
#define MEM_CMD_RSTIO 0xFF
#define MEM_CMD_RDMR 0x05
#define MEM_CMD_WRMR 0x01

extern volatile uint32_t mem_23LCV1024_addrCtr;
extern volatile uint8_t mem_23LCV1024_rwFlag;

void mem_23LCV1024_init();
uint8_t mem_23LCV1024_readStatusReg();
void mem_23LCV1024_writeStatusReg(uint8_t data);
uint32_t mem_23LCV1024_getCurrentAddress();
void mem_23LCV1024_moveToAddress();
uint8_t mem_23LCV1024_getRwStatus();
void mem_23LCV1024_setRwStatus(uint8_t status);

inline void mem_23LCV1024_writeByte (uint8_t data) {
	SPI_masterTransmit(data);
	mem_23LCV1024_addrCtr++;
}

inline uint8_t mem_23LCV1024_readByte () {
	mem_23LCV1024_addrCtr++;
	return SPI_masterReceive();
}

inline void mem_23LCV1024_writeByte_woi (uint8_t data) {
	SPI_masterTransmit(data);
}

inline uint8_t mem_23LCV1024_readByte_woi () {
	return SPI_masterReceive();
}

#endif /* MEM_23LCV1024_H_ */
