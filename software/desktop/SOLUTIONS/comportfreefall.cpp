#include "comportfreefall.h"
#include "rs232.h"

EXPORT int lib_RS232_OpenComport(int comport_number, int baudrate, const char *mode)
{
    return RS232_OpenComport(comport_number, baudrate, mode);
}

EXPORT int lib_RS232_PollComport(int comport_number, unsigned char *buf, int size)
{
    return RS232_PollComport(comport_number, buf, size);
}

EXPORT int lib_RS232_SendByte(int comport_number, unsigned char byte)
{
    return RS232_SendByte(comport_number, byte);
}

EXPORT int lib_RS232_SendBuf(int comport_number, unsigned char *buf, int size)
{
    return RS232_SendBuf(comport_number, buf, size);
}

EXPORT void lib_RS232_CloseComport(int comport_number)
{
    return RS232_CloseComport(comport_number);
}

EXPORT void lib_RS232_cputs(int comport_number, const char *text)
{
    return RS232_cputs(comport_number, text);
}

EXPORT void lib_RS232_nputs(int comport_number, const char *text)
{
    while(*text != '\n')
        RS232_SendByte(comport_number, *(text++));
}

EXPORT void lib_RS232_commlistener(int comport_number,
                                   bool * data_ready,
                                   int * error,
                                   bool * reading,
                                   unsigned char * buffer,
                                   int * buffer_index)
{
    int bytes_read = RS232_PollComport(comport_number, &buffer[*buffer_index], 4096);
    if(bytes_read != 0)
    {
        if(!*reading)
            *reading = true;
        *buffer_index += bytes_read;
        if(*buffer_index > 4095)
            *error = 1; // overflow error
    }
    else if(*reading)
    {
        *reading = false;
        if(*buffer_index < 74)
        {
            buffer[*buffer_index] = '\0';
            *data_ready = true;
        }
        else
            *error = 2; //unrecognized message
    }
    //else do nothing
}

EXPORT void lib_RS232_datalistener(int comport_number,
                                   unsigned char * buffer,
                                   int * buffer_index)
{
    int bytes_read = RS232_PollComport(comport_number, &buffer[*buffer_index], 4096);
    *buffer_index += bytes_read;
}






