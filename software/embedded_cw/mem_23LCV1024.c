/*
 * mem_23LCV1024.c
 *
 *  Created on: 14 gru 2014
 *      Author: maciek
 */

#include "mem_23LCV1024.h"
#include <avr/delay.h>

volatile uint32_t mem_23LCV1024_addrCtr = 0;
volatile uint8_t mem_23LCV1024_rwFlag = 1;

void mem_23LCV1024_init () {
	MEM_CS_DDR |= (1<<MEM_CS_PIN);
	MEM_CS_PORT |= (1<<MEM_CS_PIN);
	SPI_masterInit();
}

void mem_23LCV1024_writeStatusReg (uint8_t data) {
	
}

uint8_t mem_23LCV1024_readStatusReg () {
	
}

uint32_t mem_23LCV1024_getCurrentAddress () {
	return mem_23LCV1024_addrCtr;
}

void mem_23LCV1024_moveToAddress (uint32_t addr) {
	
}

uint8_t mem_23LCV1024_getRwStatus () {
	return mem_23LCV1024_rwFlag;
}

void mem_23LCV1024_setRwStatus (uint8_t status) {
	mem_23LCV1024_rwFlag = status;
}
