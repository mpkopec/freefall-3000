/*
 * HD44780.c
 *
 *  Created on: 16 pa� 2014
 *      Author: Maciek
 */


#include <avr/io.h>
#include <avr/delay.h>
#include "HD44780.h"

void HD44780_outPorts () {
	HD44780_DDDR = 0xff;
	HD44780_CDDR |= (1 << HD44780_E) | (1 << HD44780_RW) | (1 << HD44780_RS);
}

void HD44780_inPorts () {
	HD44780_DDDR = 0x00;
	HD44780_CDDR |= (1 << HD44780_E) | (1 << HD44780_RW) | (1 << HD44780_RS);
}

void HD44780_sendCommand (uint8_t cmd) {
	HD44780_outPorts();
	HD44780_E_SET;
	HD44780_RW_RESET;
	HD44780_RS_RESET;
	HD44780_DPORT = cmd;
	_delay_us(1);
	HD44780_E_RESET;
	_delay_us(50);
}

uint8_t HD44780_readStatus () {
	HD44780_outPorts();
	HD44780_E_SET;
	HD44780_RW_SET;
	HD44780_RS_RESET;
	_delay_us(1);
	HD44780_E_RESET;
	_delay_us(1);

	uint8_t tmp = HD44780_DPIN;
	return tmp;
}

void HD44780_sendData (uint8_t data) {
	HD44780_outPorts();
	HD44780_E_SET;
	HD44780_RW_RESET;
	HD44780_RS_SET;
	HD44780_DPORT = data;
	_delay_us(1);
	HD44780_E_RESET;
	_delay_us(50);
}

void HD44780_setAddress (uint8_t line, uint8_t charPos) {
	line &= 1;
	charPos &= 0x0f;

	uint8_t addr = (line) ? (0x40 | charPos) : charPos;

	HD44780_sendCommand(addr | 0x80);
}

void HD44780_returnHome () {
	HD44780_sendCommand(0x02);
	_delay_ms(2);
}

void HD44780_clearDisplay () {
	HD44780_sendCommand(0x01);
	_delay_ms(2);
}

void HD44780_init (uint8_t cb) {
	cb &= 0x03;
	_delay_ms(50);
	HD44780_sendCommand(0x3f);
	HD44780_sendCommand(0x3f);
	HD44780_sendCommand(0x0c | cb);
	HD44780_clearDisplay();
	HD44780_sendCommand(0x06);
	HD44780_setAddress(0, 0);
}

void HD44780_sendString (char *str) {
	uint8_t ctr = 0;

	while (*str != 0) {
		if (ctr > 15) {
			HD44780_setAddress(1, 0);
		}
		if (ctr > 31) {
			return;
		}

		HD44780_sendData(*str);
		str++;
		ctr++;
	}
}
