/*
 * main.c
 *
 *  Created on: 8 lis 2014
 *      Author: Maciek
 */

#define F_CPU 12000000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/delay.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "HD44780.h"
#include "servo.h"
#include "uart.h"
#include "mem_23LCV1024.h"
#include "adc.h"
#include "commands.h"

CommandList *cList;
Peaks *peaks = NULL;

void command_CF_ID_handler () {
	
}

void command_CF_ST_handler () {
	
}

void command_SY_TG_handler () {
	
}

void command_SY_UP_handler () {
	
}

void command_SY_DN_handler () {
	
}

void command_SY_MS_handler () {
	uint8_t statusReg = mem_23LCV1024_readStatusReg();
	UART_sendByte(statusReg);
}

void command_RQ_WF_handler () {
	UART_disableReceiverInterrupt();

	mem_23LCV1024_rwFlag = 1;
	mem_23LCV1024_moveToAddress(0UL);

	do {
		UART_sendByte(mem_23LCV1024_readByte());
	} while (mem_23LCV1024_addrCtr <= 0x1FFFFUL);

	UART_enableReceiverInterrupt();
}

void command_MS_ST_handler () {
	UART_disableReceiverInterrupt();
	mem_23LCV1024_rwFlag = 0;
	mem_23LCV1024_moveToAddress(0UL);

	state = CF_CS;
	UART_sendString(allPossibleStates[state]);
	ADC_enableInterrupt();
	ADC_setFreeRunning();
	SERVO_goDownPWM();
	ADC_startConversion();
}

void command_RQ_PK_handler () {
	if (!peaks) {
		return;
	}

	UART_disableReceiverInterrupt();

	UART_sendByte(peaks->peakCount);

	uint8_t i;
	int8_t j;
	for (i = 0; i < PEAK_COUNT; i++) {
		uint8_t *ptr = &(peaks->peakList[i]);

		for (j = 3; j >= 0; --j) {
			UART_sendByte(ptr[j]);
		}
	}

	UART_enableReceiverInterrupt();
}

void command_RQ_WP_handler () {
	command_RQ_WF_handler();
	command_RQ_PK_handler();
}

void command_CF_AK_handler () {
	free(peaks->peakList);
	free(peaks);
	peaks = NULL;
	state = CF_IL;
}

int main () {
	UART_init();
	//--------------------------
	cList = newCommandList(11);

	Command newCmd;

	newCmd.id = allPossibleCommands[CF_ID];
	newCmd.handler = command_CF_ID_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[CF_ST];
	newCmd.handler = command_CF_ST_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[SY_TG];
	newCmd.handler = command_SY_TG_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[SY_MS];
	newCmd.handler = command_SY_MS_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[RQ_WF];
	newCmd.handler = command_RQ_WF_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[MS_ST];
	newCmd.handler = command_MS_ST_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[RQ_PK];
	newCmd.handler = command_RQ_PK_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[RQ_WP];
	newCmd.handler = command_RQ_WP_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[CF_AK];
	newCmd.handler = command_CF_AK_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[SY_UP];
	newCmd.handler = command_SY_UP_handler;
	addCommand(cList, newCmd);

	newCmd.id = allPossibleCommands[SY_DN];
	newCmd.handler = command_SY_DN_handler;
	addCommand(cList, newCmd);

	//---------------------------
	UART_enableReceiverInterrupt();
	//---------------------------
	ADC_init();
	//---------------------------
	sei();
	SERVO_initOperation();
	SERVO_goUpPWM();
	//---------------------------
	mem_23LCV1024_setRwStatus(0);
	mem_23LCV1024_init();
	mem_23LCV1024_moveToAddress(0UL);
	//---------------------------

	state = CF_IL;
	
	while (1) {
		if (flagsHolder.dataAquisitionFinished) {
			flagsHolder.dataAquisitionFinished = 0;
			mem_23LCV1024_addrCtr = 0;
			uint8_t dummy = ADCH;
			state = CF_CF;
			UART_sendString(allPossibleStates[state]);

			peaks = ADC_findPeaks();

			state = CF_RD;
			UART_sendString(allPossibleStates[state]);
			UART_enableReceiverInterrupt();
			SERVO_goUpPWM();
		}

		if (flagsHolder.receivedCommand) {
			uint8_t i;

			for (i = cList->n - 1; i >= 0; --i) {
				if (!strcmp(cmd, cList->cList[i].id)) {
					cList->cList[i].handler();
					break;
				}
			}

			flagsHolder.receivedCommand = 0;
		}
	}
}
